# Docker libwebkit2gtk/stretch-backports


## Getting the Docker runtime

You can follow the [Docker's official site instructions](https://docs.docker.com/engine/install/ubuntu/)
(recommended) or follow the next steps.

You need to run docker-ce (17 or higher). Older versions of Docker were called
docker, docker.io, or docker-engine. If these are installed, uninstall them:


``` sh
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
```

Then install Docker from Docker's APT repositories:

``` sh
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```


## Run

``` sh
git clone https://gitlab.com/saavedra.pablo/docker-libwebkit2gtk.git
export URL="https://webkitgtk.org"  # or URL="inspector://127.0.0.1:12321"
export XAUTHORITY="/run/user/1000/gdm/Xauthority"  # or `xhost +`
cd docker-libwebkit2gtk
sudo -E ./run.sh
# or
sudo -E ./run.sh stretch-backports
# or
sudo -E ./run.sh buster
```

