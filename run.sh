#!/bin/bash -x

DISTRO=${1}
if [ -z "${DISTRO}" ]
then
    DISTRO="stretch-backports"
fi

echo "URL: $URL"
docker build -t libwebkit2gtk/${DISTRO} -f Dockerfile.${DISTRO} .
docker run -ti --rm \
    -e URL=$URL \
    -e DISPLAY=$DISPLAY \
    -e XDG_RUNTIME_DIR=/tmp \
    -e WAYLAND_DISPLAY=$WAYLAND_DISPLAY \
    -e XAUTHORITY=$XAUTHORITY \
    -v $XDG_RUNTIME_DIR/$WAYLAND_DISPLAY:/tmp/$WAYLAND_DISPLAY  \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -v $XAUTHORITY:$XAUTHORITY \
    --user=$(id -u $USER):$(id -g $USER) \
    --name miniBrowser libwebkit2gtk/${DISTRO}
